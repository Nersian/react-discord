# Discord Clone 

Simple Discord-clone build with React.js, Redux, MaterialUi and Firebase. Project includes basic functionalities such as login/logout with Google Authorization Provider, Creating new text channels, writing messages and posting them in realtime.<br />

Project was deployed with Firebase and can be accessed through [the following link](https://discord-clone-e8af5.web.app)<br />

In the project directory, you can run:

### `npm start`

to run the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `logging out`

To log out click your avatar icon in user info section placed in bottom-left corner. 