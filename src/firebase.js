// For Firebase JS SDK v7.20.0 and later, measurementId is optional
import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyD_n4kcnRB-1MriAObwb2Sz_qivxfU3F28",
    authDomain: "discord-clone-e8af5.firebaseapp.com",
    databaseURL: "https://discord-clone-e8af5.firebaseio.com",
    projectId: "discord-clone-e8af5",
    storageBucket: "discord-clone-e8af5.appspot.com",
    messagingSenderId: "174541349506",
    appId: "1:174541349506:web:467433a39e05b6fb324a9b",
    measurementId: "G-C4R2DMXTNH"
  };

  const firebaseApp = firebase.initializeApp(firebaseConfig);
  const db = firebaseApp.firestore();
  const auth = firebase.auth();
  const provider = new firebase.auth.GoogleAuthProvider();

  export {auth, provider}
  export default db;